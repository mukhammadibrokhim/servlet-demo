package com.example.servlet.web;

import com.example.servlet.dao.CarDao;
import com.example.servlet.models.Car;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.List;

@WebServlet(urlPatterns = "/")
public class listServlet extends HttpServlet {
    private CarDao carDao = new CarDao();
    private Gson gson = new Gson();

    @Override
    protected void doGet(
            HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String action = request.getServletPath();

        try {
            switch (action) {
                case "/new":
                    showNewForm(request, response);
                    break;
                case "/insert":
                    insertCar(request, response);
                    break;
                case "/delete":
                    deleteCar(request, response);
                    break;
                case "/edit":
                    showEditForm(request, response);
                    break;
                case "/search":
                    searchBytype(request, response);
                    break;
                case "/update":
                    updateCar(request, response);
                    break;
                default:
                    listCar(request, response);
                    break;
            }
        } catch (SQLException ex) {
            throw new ServletException(ex);
        }

    }

    @Override
    protected void doPost(
            HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        doGet(request, response);
    }


    private void listCar(HttpServletRequest request, HttpServletResponse response)
            throws SQLException, IOException, ServletException {
        System.out.println(request.getReader().toString());
        List<Car> listCar = carDao.selectAllCar();
        PrintWriter out = response.getWriter();
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        out.println(gson.toJson(listCar));
        out.flush();
    }

    private void showNewForm(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        RequestDispatcher dispatcher = request.getRequestDispatcher("Car-form.jsp");
        dispatcher.forward(request, response);
    }

    private void showEditForm(HttpServletRequest request, HttpServletResponse response)
            throws SQLException, ServletException, IOException {
        JsonObject data = new Gson().fromJson(request.getReader(), JsonObject.class);
        int id = Integer.parseInt(data.get("id").getAsString());
        Car existingCar = carDao.selectCar(id);
        PrintWriter out = response.getWriter();
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        out.println(gson.toJson(gson.toJson(existingCar)));
        out.flush();

    }

    private void insertCar(HttpServletRequest request, HttpServletResponse response)
            throws SQLException, IOException {
        JsonObject data = new Gson().fromJson(request.getReader(), JsonObject.class);
        String type = data.get("type").getAsString();
        String model = data.get("model").getAsString();
        String colour = data.get("colour").getAsString();
        int speed = Integer.parseInt(data.get("speed").getAsString());
        double maxSpeed = Double.parseDouble(data.get("maxSpeed").getAsString());
        Car newCar = new Car(type,model,colour,speed,maxSpeed);
        carDao.insertCar(newCar);
        response.sendRedirect("list");
    }

    private void updateCar(HttpServletRequest request, HttpServletResponse response)
            throws SQLException, IOException {
        JsonObject data = new Gson().fromJson(request.getReader(), JsonObject.class);

        System.out.printf("data" ,data);
        long id = Long.parseLong(data.get("id").getAsString());
        String type = data.get("type").getAsString();
        String model = data.get("model").getAsString();
        String colour = data.get("colour").getAsString();
        int speed = Integer.parseInt(data.get("speed").getAsString());
        double maxSpeed = Double.parseDouble(data.get("maxSpeed").getAsString());

        Car car = new Car(id,type,model,colour,speed,maxSpeed);
        carDao.updateCar(car);
        response.sendRedirect("list");
    }

    private void deleteCar(HttpServletRequest request, HttpServletResponse response)
            throws SQLException, IOException {
        JsonObject data = new Gson().fromJson(request.getReader(), JsonObject.class);

        int id = Integer.parseInt(data.get("id").getAsString());
        carDao.deleteCar(id);
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        response.sendRedirect("list");

    }

    private void searchBytype(HttpServletRequest request, HttpServletResponse response)
            throws SQLException, IOException {
        JsonObject data = new Gson().fromJson(request.getReader(), JsonObject.class);
        String type = data.get("type").getAsString();
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        List<Car> carList = carDao.searchWithType(type);
        PrintWriter printWriter = response.getWriter();
        if (carList.size() == 0){
            printWriter.print("object Yoq!");
            printWriter.flush();

        }
        printWriter.println(gson.toJson(carList));
        printWriter.flush();
        response.sendRedirect("list");

    }
}
