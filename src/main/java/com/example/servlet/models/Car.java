package com.example.servlet.models;

public class Car {
    private Long id;
    private String type;
    private String model;
    private String colour;
    private int speed;
    private double maxSpeed;

    public Car( String type, String model, String colour, int speed, double maxSpeed) {
        this.type = type;
        this.model = model;
        this.colour = colour;
        this.speed = speed;
        this.maxSpeed = maxSpeed;
    }

    public Car(Long id, String type, String model, String colour, int speed, double maxSpeed) {
        this.id = id;
        this.type = type;
        this.model = model;
        this.colour = colour;
        this.speed = speed;
        this.maxSpeed = maxSpeed;
    }

    public Car() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getColour() {
        return colour;
    }

    public void setColour(String colour) {
        this.colour = colour;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public double getMaxSpeed() {
        return maxSpeed;
    }

    public void setMaxSpeed(double maxSpeed) {
        this.maxSpeed = maxSpeed;
    }
}
