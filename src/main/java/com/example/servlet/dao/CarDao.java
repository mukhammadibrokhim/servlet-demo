package com.example.servlet.dao;


import com.example.servlet.models.Car;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class CarDao {
    private String dbUrl = "jdbc:postgresql:5432//localhost/test";
    private String dbUsername = "test_user";
    private String dbPassword = "test123";


    private static final String INSERT_CAR_SQL = "INSERT INTO car" + "  (type, model, colour, speed, maxSpeed) VALUES "
            + " (?, ?, ?, ?, ?);";

    private static final String SELECT_CAR_BY_ID = "select id,type ,model,colour,speed, maxSpeed from car where id =?";
    private static final String SELECT_CAR_ALL = "select * from car order by id desc";
    private static final String DELETE_CAR_SQL = "delete from car where id = ?;";
    private static final String UPDATE_CAR_SQL = "update car set type = ?,model= ?, colour =?, speed=?,maxSpeed=? where id = ?;";
    private static final String SELECT_BY_TYPE = "SELECT * FROM car WHERE type=?";

    protected Connection getConnection() {
        Connection connection = null;
        try {
            Class.forName("org.postgresql.Driver");
            connection = DriverManager.getConnection(
                    "jdbc:postgresql://127.0.0.1:5432/test", "test_user", "test123"
            );
            System.out.println("Connected");
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return connection;
    }

    public void insertCar(Car car) throws SQLException {
        System.out.println(INSERT_CAR_SQL);
        // try-with-resource statement will auto close the connection.
        try (Connection connection = getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(INSERT_CAR_SQL)) {
            preparedStatement.setString(1, car.getType());
            preparedStatement.setString(2, car.getModel());
            preparedStatement.setString(3, car.getColour());
            preparedStatement.setInt(4, car.getSpeed());
            preparedStatement.setDouble(5, car.getMaxSpeed());
            System.out.println(preparedStatement);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            printSQLException(e);
        }
    }


    public List<Car> selectAllCar() {
        List<Car> carList = new ArrayList<>();
        try (Connection connection = getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SELECT_CAR_ALL)) {
            System.out.println(preparedStatement);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                long id = rs.getInt("id");
                String type = rs.getString("type");
                String model = rs.getString("model");
                String colour = rs.getString("colour");
                Integer speed = rs.getInt("speed");
                Double maxSpeed = rs.getDouble("maxSpeed");
                carList.add(new Car(id, type, model, colour, speed, maxSpeed));
            }
        } catch (SQLException e) {
            printSQLException(e);
        }
        return carList;
    }


    public Car selectCar(int id) {
        Car car = null;
        try (Connection connection = getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SELECT_CAR_BY_ID);) {
            preparedStatement.setInt(1, id);
            System.out.println(preparedStatement);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                (id) = rs.getInt("id");
                String type = rs.getString("type");
                String model = rs.getString("model");
                String colour = rs.getString("colour");
                Integer speed = rs.getInt("speed");
                Double maxSpeed = rs.getDouble("maxSpeed");
                car = new Car((long) id, type, model, colour, speed, maxSpeed);
            }
        } catch (SQLException e) {
            printSQLException(e);
        }
        return car;
    }

    public boolean deleteCar(int id) throws SQLException {
        boolean rowDeleted;
        try (Connection connection = getConnection();
             PreparedStatement statement = connection.prepareStatement(DELETE_CAR_SQL);) {
            statement.setInt(1, id);
            rowDeleted = statement.executeUpdate() > 0;
        }
        return rowDeleted;
    }

    private void printSQLException(SQLException ex) {
        for (Throwable e : ex) {
            if (e instanceof SQLException) {
                e.printStackTrace(System.err);
                System.err.println("SQLState: " + ((SQLException) e).getSQLState());
                System.err.println("Error Code: " + ((SQLException) e).getErrorCode());
                System.err.println("Message: " + e.getMessage());
                Throwable t = ex.getCause();
                while (t != null) {
                    System.out.println("Cause: " + t);
                    t = t.getCause();
                }
            }
        }
    }

    public List<Car> searchWithType(String type) {
        List<Car> carList = new ArrayList<>();
        try (Connection connection = getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SELECT_BY_TYPE)) {
            preparedStatement.setString(1,type);
            System.out.println(preparedStatement);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                long id = rs.getInt("id");
                type = rs.getString("type");
                String model = rs.getString("model");
                String colour = rs.getString("colour");
                Integer speed = rs.getInt("speed");
                Double maxSpeed = rs.getDouble("maxSpeed");
                carList.add(new Car(id, type, model, colour, speed, maxSpeed));
            }
        } catch (SQLException e) {
            printSQLException(e);
        }
        return carList;

    }

    public void updateCar(Car car) {
        try (Connection connection = getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_CAR_SQL)) {
            preparedStatement.setString(1, car.getType());
            preparedStatement.setString(2, car.getModel());
            preparedStatement.setString(3, car.getColour());
            preparedStatement.setInt(4, car.getSpeed());
            preparedStatement.setDouble(5, car.getMaxSpeed());
            preparedStatement.setLong(6, car.getId());
            System.out.println(preparedStatement);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            printSQLException(e);
        }

    }
}
